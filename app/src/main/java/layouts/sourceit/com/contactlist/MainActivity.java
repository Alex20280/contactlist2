package layouts.sourceit.com.contactlist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name;
    EditText email;
    ImageView image;
    ListView list;
    LinearLayout itemMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        image = (ImageView) findViewById(R.id.image);
        list = (ListView) findViewById(R.id.list);
        itemMain = (LinearLayout) findViewById(R.id.itemMain);


        MySimpleArrayAdapter mySimpleArrayAdapter = new MySimpleArrayAdapter(this, generate());
        list.setAdapter(mySimpleArrayAdapter);


        itemMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("email", email.getText().toString());
                intent.putExtra("Image", R.drawable.contact1);
                startActivity(intent);
            }
        });

    }
    private ArrayList<User> generate(){
        ArrayList<User> list = new ArrayList();
        for (int i=0; i<30; i++){
            list.add(new User("Alex"+i, i+"alex@gmail.com", R.drawable.contact1+i));
        }
        return list;
    }


    public class MySimpleArrayAdapter extends ArrayAdapter<String> {
        public MySimpleArrayAdapter(Context context, ArrayList list) {
            super(context, R.layout.item, list);

        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                rowView = getLayoutInflater().inflate(R.layout.item, null, true);
                holder = new ViewHolder();
                holder.name = (EditText) rowView.findViewById(R.id.name);
                holder.email = (EditText) rowView.findViewById(R.id.email);
                holder.image = (ImageView) rowView.findViewById(R.id.image);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }
            //User user = list.get(position);
            //holder.name.setText(user.getName);
            //holder.email.setText(user.getEmail);
            //holder.image.setImageResource(user.getImage);
            return rowView;
        }
    }


    static class ViewHolder {
        public EditText name;
        public EditText email;
        public ImageView image;
    }

}

