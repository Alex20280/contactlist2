package layouts.sourceit.com.contactlist;


public class User{
    private String name;
    private String email;
    private Integer image;

    public User(String name, String email, Integer image) {
        this.name = name;
        this.email = email;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Integer getImage() {
        return image;
    }
}
